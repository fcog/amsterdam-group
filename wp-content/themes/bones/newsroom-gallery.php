<?php
/*
Template Name: Newsroom Gallery
*/
?>

<?php get_header(); ?>

<div id="info-container" class="container_24">

  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

      <h2><?php the_title(); ?></h2>

      <?php get_template_part( 'newsroom', 'sidebar' ); ?> 
      
      <div class="transparent-content-content grid_18 omega" >
        <?php the_content(); ?>
      </div>

  <?php endwhile; endif ?>

</div>  

<?php get_footer(); ?>
