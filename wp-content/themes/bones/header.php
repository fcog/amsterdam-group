<!doctype html>
<html lang="en">
<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

	<head>
		<meta charset="utf-8">

		<!-- Google Chrome Frame for IE -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title><?php wp_title('&laquo;', true, 'right'); ?><?php bloginfo('name'); ?></title>

		<!-- mobile meta (hooray!) -->
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

		<!-- icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) -->
		<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-icon-touch.png">
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->
		<!-- or, set /favicon.ico for IE10 win -->
		<meta name="msapplication-TileColor" content="#f01d4f">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

	    <!-- Google Fonts -->
	    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>

	    <!--jQuery-->
	    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>

	    <!--UI theme roller-->
		<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/library/css/custom-theme/jquery-ui-1.8.24.custom.css" />

	    <!--Styles-->
		<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/library/css/960.css" />

	    <!--Only Mac - Safari Class-->
	    <script type="text/javascript">
	    jQuery(function(){
	        // console.log(navigator.userAgent);
	        /* Adjustments for Safari on Mac */
	        if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Mac') != -1) {
	            jQuery('html').addClass('mac'); // provide a class for the safari-mac specific css to filter with
	        }
	    });
	    </script>

	    <!--Selectivizr-->
	    <!--[if (gte IE 6)&(lte IE 8)]>
	        <script type="text/javascript" src="js/selectivizr.js"></script>
	        <noscript><link rel="stylesheet" href="css/style.css" /></noscript>
	    <![endif]-->

	    <!--[if IE]>
	        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	    <![endif]-->

		<!-- wordpress head functions -->
		<?php wp_head(); ?>
		<!-- end of wordpress head -->

		<!-- drop Google Analytics Here -->
		<!-- end analytics -->

	</head>

	<body <?php body_class(); ?>>

	<div id="wrap">

		<header id="header"> 
	    	<div class="container_24">
	        	<div class="title-site grid_15">
	                <h1 class="branding grid_5"><a href="<?php echo home_url(); ?>" class="logo"><img src="<?php echo get_template_directory_uri(); ?>/library/images/Amsterdam-group-logo.png"></a></h1>
	                <h2 class="grid_11 prefix_5">
	                	<?php
		                if (is_active_sidebar('header-slogan')) :
		                    dynamic_sidebar('header-slogan');
		                endif;
		                ?>
	                </h2>
	            </div>
	        	<div id="follow_us" class="grid_7 prefix_1 suffix_1">
            		<?php
	                if (is_active_sidebar('header-social-media')) :
	                    dynamic_sidebar('header-social-media');
	                endif;
	                ?>
	            </div>
	        </div>
		</header>

	    <div id="content">
	    	<div class="container_24">
	        	<nav id="nav" class="grid_23 alpha">
					<?php bones_main_nav(); ?>
	            </nav>
	        </div>	