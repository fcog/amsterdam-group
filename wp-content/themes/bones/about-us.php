<?php
/*
Template Name: About Us
*/
?>

<?php get_header(); ?>

<div id="info-container" class="container_24">

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <h2 class="page-title"><?php the_title(); ?></h2>

    <div class="white-content">
        <p><?php the_content(); ?></p>

        <div id="leadership-section">
          <h3>Leadership</h3>

            <?php
            if (is_active_sidebar('about-us-leadership')) :
                dynamic_sidebar('about-us-leadership');
            endif;
            ?> 
        </div>    
    </div>

    <?php endwhile; endif ?>

    <?php echo do_shortcode('[tiny-carousel-slider id="1"]'); ?>

</div>



<?php get_footer(); ?>
