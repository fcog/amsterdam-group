<?php
/*
Template Name: Services Internal
*/
?>

<?php get_header(); ?>

<div id="info-container" class="container_24">

  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <h2>Services</h2>
    <section class="sidebar">
    <?php
        services_menu(); 
    ?>
    </section>
    <div class="white-content grid_16 alpha omega" >
      <h3 class="services-title">
        <?php 
        if (has_post_thumbnail()) the_post_thumbnail();  
        the_title(); 
        ?>
      </h3>
      <p><?php the_content(); ?></p>
      <a href="../services" title="back to About us" class="green-buttom">Back</a>
    </div>

  <?php endwhile; endif ?>

</div>  

<?php get_footer(); ?>
