<?php
/*
Template Name: Newsroom Blog
*/
?>

<?php get_header(); ?>

<div id="info-container" class="container_24">

      <h2><?php the_title(); ?></h2>
      
      <?php get_template_part( 'newsroom', 'sidebar' ); ?> 

      <div class="transparent-content-content grid_18 omega" >
        <div class="news-content">
          <div class="latest-news-slider">        
            <?php
              $temp = $wp_query;
              $wp_query= null;
              $wp_query = new WP_Query();
              $wp_query->query('cat=1&showposts=10'.'&paged='.$paged);
              ?>

              <?php while ($wp_query->have_posts()): $wp_query->the_post(); ?>

                <article class="blog-box<?php if ($i == 3) echo " last"?>">
                  <h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                  <span>1 May, 2013, by<a href="#">Sam Amsterdam</a></span>
                  <?php the_excerpt(); ?>
                  <a href="<?php echo get_permalink(); ?>" title="Read More">Read More</a>
                  <div class="share"><?php echo do_shortcode('[ssba]'); ?></div>
                </article>

              <?php endwhile; ?>

          </div>
        </div>

          <div class="navigation">
          <div class="alignleft"><? next_posts_link('&larr; Older Entries') ?></div>
          <div class="alignright"><? previous_posts_link('Newer Entries &rarr;') ?></div>
          </div>
      </div>

</div>  

<?php get_footer(); ?>
