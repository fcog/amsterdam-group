<?php
/*
Template Name: Our Partners
*/
?>

<?php get_header(); ?>

<script type="text/javascript"
  src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBC0eBn483OUFNdv1EdEulKfeNAi1N-gmg&sensor=false">
</script>


<script>
var map;
var MY_MAPTYPE_ID = 'custom_style';
var markersArray = [];

function clearOverlays() {
  for (var i = 0; i < markersArray.length; i++ ) {
    markersArray[i].setMap(null);
  }
  markersArray = [];
}

function addMarker(location) {
  marker = new google.maps.Marker({
    position: location,
    map: map,
    icon: '<?php echo get_template_directory_uri(); ?>/library/images/pin-map.png'
  });
  markersArray.push(marker);
}

function initialize() {

    var featureOpts = [
    {
      featureType: "landscape",
      stylers: [
        { color: '#b4b4b4' }
      ]
    },
    {
      featureType: "all",
      elementType: 'labels',
      stylers: [
        { visibility: 'off' }
      ]
    },     
    {
      featureType: "road",
      stylers: [
        { visibility: 'off' }
      ]
    },       
    {
      featureType: "poi.park",
      stylers: [
        { visibility: 'off' }
      ]
    },   
    {
      featureType: "administrative.locality",
      elementType: 'labels',
      stylers: [
        { visibility: 'on' }
      ]
    },    
    {
      featureType: "administrative.country",
      elementType: 'labels',
      stylers: [
        { visibility: 'on' }
      ]
    },
    {
      featureType: 'water',
      stylers: [
        { color: '#f0f2f1' }
      ]
    }
  ];

  var mapOptions = {
    zoom: 2,
    center: new google.maps.LatLng(20,0),
    // mapTypeId: google.maps.MapTypeId.ROADMAP,
    mapTypeId: MY_MAPTYPE_ID,
    panControl: false,
    zoomControl: true,
    mapTypeControl: false,
    scaleControl: false,
    streetViewControl: false,
    overviewMapControl: false,
    zoomControlOptions: {
      style: google.maps.ZoomControlStyle.SMALL
    },
    // draggable: false,
    scrollwheel: false,
  };
  map = new google.maps.Map(document.getElementById('map'),
      mapOptions);  

  var styledMapOptions = {
    name: 'Custom Style'
  };

  var customMapType = new google.maps.StyledMapType(featureOpts, styledMapOptions);

  map.mapTypes.set(MY_MAPTYPE_ID, customMapType);

  <?php
  $temp = $wp_query;
  $wp_query= null;
  $wp_query = new WP_Query();
  $wp_query->query(array('post_type' => 'partners', 'posts_per_page' => '-1'));
  ?>

  <?php while ($wp_query->have_posts()): $wp_query->the_post(); ?>
    addMarker( new google.maps.LatLng(<?php echo get_field('latitude'); ?>, <?php echo get_field('longitude'); ?>) );
  <?php endwhile; ?>  
}

google.maps.event.addDomListener(window, 'load', initialize);


// LOAD CLIENTS CONTENT
jQuery(document).ready(function(){
  jQuery('.client-box').click(function(){
    var lat = jQuery(this).attr('data-latitude');
    var lng = jQuery(this).attr('data-longitude');
    jQuery.get(jQuery(this).attr('data-permalink'), function(data){
      if (jQuery('#client-info .description').length > 0) jQuery('#client-info .description').remove();
      jQuery("<div class='description'><div class='title'>"+jQuery(data).find('.entry-title').html()+"</div>"+jQuery(data).find('.entry-content').html()+"</div>").insertAfter('#map');
      if (jQuery('#ssba').length > 0) jQuery('#ssba').remove();
      jQuery('#map').css('float', 'left').css('width','70%');
      google.maps.event.trigger(map, 'resize');
      clearOverlays();
      addMarker(new google.maps.LatLng(lat, lng));
      map.setCenter(new google.maps.LatLng(lat,lng));
      map.setZoom(6);  
    });
  });
});


</script>


<div id="info-container" class="container_24">
  <h2>Our Partners</h2>
    <div id="client-info">
      <div class="map-container" id="map">
      </div>
    </div>
    <ul class="clients">
      <?php
      $temp = $wp_query;
      $wp_query= null;
      $wp_query = new WP_Query();
      $wp_query->query(array('post_type' => 'partners', 'posts_per_page' => '-1'));
      ?>

      <?php 
      $total_clients_boxes=0; 
      while ($wp_query->have_posts()): $wp_query->the_post();
        $total_clients_boxes++;
        ?>
        <li><div class="client-box" data-permalink="<?php the_permalink(); ?>" data-latitude="<?php echo get_field('latitude'); ?>" data-longitude="<?php echo get_field('longitude'); ?>"><?php if (has_post_thumbnail()) the_post_thumbnail('newsroom-thumb'); else echo the_title(); ?></div></li>
        <?php 
      endwhile;

      //each row needs to have 4 boxes
      //count how many left boxes need to be printed and if the result is lower than 4 then print white boxes
      $total_boxes_needed = $total_clients_boxes % 4;

      if ($total_boxes_needed % 4){
        $i=0;
        while ($i + $total_boxes_needed < 4){
          ?>
          <li><div></div></li>
          <?php
          $i++;
        }
      }
      ?>
    </ul>
</div>

<?php get_footer(); ?>
