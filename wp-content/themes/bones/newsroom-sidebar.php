<section class="sidebar">
  <?php newsroom_menu(); ?>
  <div class="newsletter-box">
    <h5>Newsletter</h5>
      <?php
      if (is_active_sidebar('newsroom-newsletter')) :
          dynamic_sidebar('newsroom-newsletter');
      endif;
      ?> 
  </div>
  <div class="news-contact-info">
    <h5>News contact</h5>
    <?php
    if (is_active_sidebar('newsroom-contact')) :
        dynamic_sidebar('newsroom-contact');
    endif;
    ?> 
  </div> 
</section>