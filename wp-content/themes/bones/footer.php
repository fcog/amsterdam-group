		</div>

		<footer id="footer">
	        <div class="container_24">
	        	<div id="logo_footer"><img src="<?php echo get_template_directory_uri(); ?>/library/images/logo-footer.png"></div>
	            <?php bones_footer_links(); ?>
	            <p>© Copyright Amsterdam Group 2013</p>
	        </div>
		</footer>			

		<!-- all js scripts are loaded in library/bones.php -->
		<?php wp_footer(); ?>

	</div>	

	</body>

</html> <!-- end page. what a ride! -->
