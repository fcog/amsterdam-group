<?php
/*
Template Name: Newsroom
*/
?>

<?php get_header(); ?>

<div id="info-container" class="container_24">

  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

      <h2><?php the_title(); ?></h2>

      <?php get_template_part( 'newsroom', 'sidebar' ); ?> 

          <div class="transparent-content-content grid_18 omega" >
            <div class="news-content">
              <div class="latest-news-slider">

                    <?php
                    $args = array( 'numberposts' => 1, 'category' => 7, );
                    $lastposts = get_posts( $args );
                    foreach($lastposts as $post) : setup_postdata($post); ?>
                      <?php if (has_post_thumbnail()): ?> <div class="pic"><?php the_post_thumbnail('newsroom-thumb'); ?></div><?php endif ?>
                      <div class="latest-news-info">
                        <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                        <span><?php the_date( ); ?> by<a href="#"><?php the_author(); ?></a></span>
                        <?php the_excerpt(); ?>
                        <a href="<?php echo get_permalink(); ?>" title="Read More">Read More</a>
                        <div class="share"><?php echo do_shortcode('[ssba]'); ?></div>
                      </div>
                    <?php endforeach; ?>
          
                </div>
              <section>
                  <?php 
                  $args = array( 'numberposts' => 3, 'category__in' => array( 1 ), 'category__not_in' => array( 7 ) );
                  $lastposts = get_posts( $args );

                  if (count($lastposts) > 0):
                  ?>
                    <h3>Blog</h3>
                    <a href="blog" class="view">View all</a>
                    <?php
                    $i=0;
                    foreach($lastposts as $post) : setup_postdata($post); 
                      $i++;
                    ?>                  
                      <article class="blog-box<?php if ($i == 3) echo " last"?>">
                        <h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                        <span>1 May, 2013, by<a href="#">Sam Amsterdam</a></span>
                        <?php the_excerpt(); ?>
                        <a href="<?php echo get_permalink(); ?>" title="Read More">Read More</a>
                        <div class="share"><?php echo do_shortcode('[ssba]'); ?></div>
                      </article>
                    <?php endforeach; ?>
                  <?php endif ?>
                </section>
              <section>
                  <?php 
                  $args = array( 'numberposts' => 3, 'category__in' => array( 8 ), 'category__not_in' => array( 7 ) );
                  $lastposts = get_posts( $args ); 
                  if (count($lastposts)  > 0):
                  ?>
                    <h3>Our Clients in the Media</h3>
                    <a href="our-clients-in-the-media" class="view">View all</a>
                    <?php
                    
                    $i=0;
                    foreach($lastposts as $post) : setup_postdata($post); 
                      $i++;
                    ?>                  
                      <article class="blog-box<?php if ($i == 3) echo " last"?>">
                        <h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                        <span>1 May, 2013, by<a href="#">Sam Amsterdam</a></span>
                        <?php the_excerpt(); ?>
                        <a href="<?php echo get_permalink(); ?>" title="Read More">Read More</a>
                        <div class="share"><?php echo do_shortcode('[ssba]'); ?></div>
                      </article>
                    <?php endforeach; ?>
                  <?php endif ?>
                </section>                
                <section>
                  <h3>Media</h3>
                  <a href="media-gallery" class="view">View all</a>
                    <?php
                    if (is_active_sidebar('newsroom-gallery')) :
                        dynamic_sidebar('newsroom-gallery');
                    endif;
                    ?> 
                </section>
            </div>
            </div>

  <?php endwhile; endif ?>

</div>  

<script type="text/javascript">
function equalHeight(group) {
  var tallest = 0;
  group.each(function() {
    var thisHeight = $(this).height();
    if(thisHeight > tallest) {
      tallest = thisHeight;
    }
  });
  group.height(tallest+35);
}
$(document).ready(function() {
  equalHeight($(".blog-box"));
});
</script>

<?php get_footer(); ?>
