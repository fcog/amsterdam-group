<?php
/*
Template Name: Homepage
*/
?>

<?php get_header(); ?>

    <div id="map-container">
    	<div id="slider-container" class="container_24">
    		<?php echo do_shortcode('[nggallery id=1 template="sliderview"]'); ?>
    	</div>
    </div>

    <section id="summarize">
        <div class="container_24">
            <?php
            if (is_active_sidebar('home-links')) :
                dynamic_sidebar('home-links');
            endif;
            ?>
            <?php
            if (is_active_sidebar('home-links-slider')) :
                dynamic_sidebar('home-links-slider');
            endif;
            ?>
        </div>	
	</section>

	<section id="feed">
        <div class="container_24">
            <div class="tweet-icon"><img src="<?php echo get_template_directory_uri(); ?>/library/images/twitter-icon.png"></div>
            <div id="twitter1" class="twitter-boxes">
                <h4>Amsterdam Group</h4>
                <h5>Social Media Feeds</h5>
                <?php
                if (is_active_sidebar('home-twitter-sam-amsterdam')) :
                    dynamic_sidebar('home-twitter-sam-amsterdam');
                endif;
                ?>
            </div>
            <div id="twitter2" class="twitter-boxes">
                <h4>Top Web 2.0 Politicians</h4>
                <h5>Write, Connect and Converse</h5>
                <?php
                if (is_active_sidebar('home-twitter-politicians')) :
                    dynamic_sidebar('home-twitter-politicians');
                endif;
                ?>
            </div>
            <div id="twitter2" class="twitter-boxes">
                <h4>Newsflash</h4>
                <h5>What We're Following,<br /> How We're Responding</h5>                
                <?php
                if (is_active_sidebar('home-twitter-newsflash')) :
                    dynamic_sidebar('home-twitter-newsflash');
                endif;
                ?>
            </div>                        
        </div>
	</section>

<?php get_footer(); ?>
