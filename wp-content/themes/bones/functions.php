<?php
/*
Author: Eddie Machado
URL: htp://themble.com/bones/

This is where you can drop your custom functions or
just edit things like thumbnail sizes, header images,
sidebars, comments, ect.
*/

/************* INCLUDE NEEDED FILES ***************/

/*
1. library/bones.php
	- head cleanup (remove rsd, uri links, junk css, ect)
	- enqueueing scripts & styles
	- theme support functions
	- custom menu output & fallbacks
	- related post function
	- page-navi function
	- removing <p> from around images
	- customizing the post excerpt
	- custom google+ integration
	- adding custom fields to user profiles
*/
require_once('library/bones.php'); // if you remove this, bones will break
/*
2. library/custom-post-type.php
	- an example custom post type
	- example custom taxonomy (like categories)
	- example custom taxonomy (like tags)
*/
require_once('library/custom-post-type.php'); // you can disable this if you like
/*
3. library/admin.php
	- removing some default WordPress dashboard widgets
	- an example custom dashboard widget
	- adding custom login css
	- changing text in footer of admin
*/
// require_once('library/admin.php'); // this comes turned off by default
/*
4. library/translation/translation.php
	- adding support for other languages
*/
// require_once('library/translation/translation.php'); // this comes turned off by default

/************* THUMBNAIL SIZE OPTIONS *************/

// Thumbnail sizes
add_image_size( 'bones-thumb-600', 600, 150, true );
add_image_size( 'bones-thumb-300', 300, 100, true );
add_image_size( 'newsroom-thumb', 306, 296, true );
/*
to add more sizes, simply copy a line from above
and change the dimensions & name. As long as you
upload a "featured image" as large as the biggest
set width or height, all the other sizes will be
auto-cropped.

To call a different size, simply change the text
inside the thumbnail function.

For example, to call the 300 x 300 sized image,
we would use the function:
<?php the_post_thumbnail( 'bones-thumb-300' ); ?>
for the 600 x 100 image:
<?php the_post_thumbnail( 'bones-thumb-600' ); ?>

You can change the names and dimensions to whatever
you like. Enjoy!
*/

/************* ACTIVE SIDEBARS ********************/

// Sidebars & Widgetizes Areas
function bones_register_sidebars() {

/************* HEADER *********************/

    register_sidebar( array(
      'id'            => 'header-slogan',
      'name'          => __( 'Header - Slogan', 'bonestheme' ),
      'before_widget'  => '',
      'after_widget'   => '',      
    ) );

    register_sidebar( array(
      'id'            => 'header-social-media',
      'name'          => __( 'Header - Social Media', 'bonestheme' ),
      'before_title'  => '',
      'after_title'   => '',      
    ) );

/************* HOME WIDGET *********************/

    register_sidebar( array(
      'id'            => 'home-links',
      'name'          => __( 'Home - Links', 'bonestheme' ),
      'before_title'  => '<div class="header"><div class="title">',
      'after_title'   => '</div></div>',
    ) );

    register_sidebar( array(
      'id'            => 'home-links-slider',
      'name'          => __( 'Home - Links Slider', 'bonestheme' ),
      'before_title'  => '<div class="header"><div class="title">',
      'after_title'   => '</div></div><div class="textwidget">',
      'after_widget'  => '<a href="" class="read-more">View all</a></div>',
    ) );

    register_sidebar( array(
      'id'            => 'home-twitter-sam-amsterdam',
      'name'          => __( 'Home - Twitter Sam Ams', 'bonestheme' ),
    ) );

    register_sidebar( array(
      'id'            => 'home-twitter-politicians',
      'name'          => __( 'Home - Twitter Politicias', 'bonestheme' ),
    ) );

    register_sidebar( array(
      'id'            => 'home-twitter-newsflash',
      'name'          => __( 'Home - Twitter Newsflash', 'bonestheme' ),
    ) );

/************* ABOUT US WIDGETS *********************/

    register_sidebar( array(
      'id'            => 'about-us-leadership',
      'name'          => __( 'About Us - Leadership boxes', 'bonestheme' ),
      'before_widget'  => '',
      'after_widget'   => '',   
    ) );

/************* SERVICES WIDGETS *********************/

    register_sidebar( array(
      'id'            => 'services',
      'name'          => __( 'Services', 'bonestheme' ),
      'before_widget'  => '',
      'after_widget'   => '',   
    ) );          

/************* CONTACT US WIDGETS *********************/

    register_sidebar( array(
      'id'            => 'contact-us-form',
      'name'          => __( 'Contact Us - Form', 'bonestheme' ),
    ) );    

    register_sidebar( array(
      'id'            => 'contact-us-info',
      'name'          => __( 'Contact Us - Info', 'bonestheme' ),
      'before_title'  => '<h3>',
      'after_title'  => '</h3>',
    ) ); 

/************* NEWSROOM WIDGETS *********************/

    register_sidebar( array(
      'id'            => 'newsroom-newsletter',
      'name'          => __( 'Newsroom - Newsletter', 'bonestheme' ),
      'before_widget'  => '',
      'after_widget'   => '',      
    ) ); 

    register_sidebar( array(
      'id'            => 'newsroom-contact',
      'name'          => __( 'Newsroom - Contact', 'bonestheme' ),
      'before_title' => '<p><strong>',
      'after_title' => '</strong></p>',
      'before_widget'  => '',
      'after_widget'   => '',
    ) );    
    
    register_sidebar( array(
      'id'            => 'newsroom-gallery',
      'name'          => __( 'Newsroom - Gallery', 'bonestheme' ),
    ) );  

	/*
	to add more sidebars or widgetized areas, just copy
	and edit the above sidebar code. In order to call
	your new sidebar just use the following code:

	Just change the name to whatever your new
	sidebar's id is, for example:

	register_sidebar(array(
		'id' => 'sidebar2',
		'name' => __('Sidebar 2', 'bonestheme'),
		'description' => __('The second (secondary) sidebar.', 'bonestheme'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

	To call the sidebar in your template, you can just copy
	the sidebar.php file and rename it to your sidebar's name.
	So using the above example, it would be:
	sidebar-sidebar2.php

	*/
} // don't remove this bracket!

/************* COMMENT LAYOUT *********************/

// Comment Layout
function bones_comments($comment, $args, $depth) {
   $GLOBALS['comment'] = $comment; ?>
	<li <?php comment_class(); ?>>
		<article id="comment-<?php comment_ID(); ?>" class="clearfix">
			<header class="comment-author vcard">
				<?php
				/*
					this is the new responsive optimized comment image. It used the new HTML5 data-attribute to display comment gravatars on larger screens only. What this means is that on larger posts, mobile sites don't have a ton of requests for comment images. This makes load time incredibly fast! If you'd like to change it back, just replace it with the regular wordpress gravatar call:
					echo get_avatar($comment,$size='32',$default='<path_to_url>' );
				*/
				?>
				<!-- custom gravatar call -->
				<?php
					// create variable
					$bgauthemail = get_comment_author_email();
				?>
				<img data-gravatar="http://www.gravatar.com/avatar/<?php echo md5($bgauthemail); ?>?s=32" class="load-gravatar avatar avatar-48 photo" height="32" width="32" src="<?php echo get_template_directory_uri(); ?>/library/images/nothing.gif" />
				<!-- end custom gravatar call -->
				<?php printf(__('<cite class="fn">%s</cite>', 'bonestheme'), get_comment_author_link()) ?>
				<time datetime="<?php echo comment_time('Y-m-j'); ?>"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php comment_time(__('F jS, Y', 'bonestheme')); ?> </a></time>
				<?php edit_comment_link(__('(Edit)', 'bonestheme'),'  ','') ?>
			</header>
			<?php if ($comment->comment_approved == '0') : ?>
				<div class="alert alert-info">
					<p><?php _e('Your comment is awaiting moderation.', 'bonestheme') ?></p>
				</div>
			<?php endif; ?>
			<section class="comment_content clearfix">
				<?php comment_text() ?>
			</section>
			<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
		</article>
	<!-- </li> is added by WordPress automatically -->
<?php
} // don't remove this bracket!

/************* SEARCH FORM LAYOUT *****************/

// Search Form
function bones_wpsearch($form) {
	$form = '<form role="search" method="get" id="searchform" action="' . home_url( '/' ) . '" >
	<label class="screen-reader-text" for="s">' . __('Search for:', 'bonestheme') . '</label>
	<input type="text" value="' . get_search_query() . '" name="s" id="s" placeholder="'.esc_attr__('Search the Site...','bonestheme').'" />
	<input type="submit" id="searchsubmit" value="'. esc_attr__('Search') .'" />
	</form>';
	return $form;
} // don't remove this bracket!


/************* ABOUT US LEADERSHIP BOXES *****************/

class LeadershipBoxWidget extends WP_Widget {
         public function __construct() {
               parent::WP_Widget(false,'Leadership Box','description=About us Leadership Boxes');
        }

        public function form( $instance ) {
			$instance = wp_parse_args( (array) $instance, array( 'image' => '', 'name' => '', 'role' => '', 'role2' => '', 'link' => '' ) );
			    $image = $instance['image'];
			    $name = $instance['name'];
			    $role = $instance['role'];
			    $role2 = $instance['role2'];
			    $link = $instance['link'];
			?>
			  <p><label for="<?php echo $this->get_field_id('image'); ?>">Image: <input class="widefat" id="<?php echo $this->get_field_id('image'); ?>" name="<?php echo $this->get_field_name('image'); ?>" type="text" value="<?php echo attribute_escape($image); ?>" /></label></p>

			  <p><label for="<?php echo $this->get_field_id('name'); ?>">Name: <input class="widefat" id="<?php echo $this->get_field_id('name'); ?>" name="<?php echo $this->get_field_name('name'); ?>" type="text" value="<?php echo attribute_escape($name); ?>" /></label></p>

			  <p><label for="<?php echo $this->get_field_id('role'); ?>">Role: <input class="widefat" id="<?php echo $this->get_field_id('role'); ?>" name="<?php echo $this->get_field_name('role'); ?>" type="text" value="<?php echo attribute_escape($role); ?>" /></label></p>

			  <p><label for="<?php echo $this->get_field_id('role2'); ?>">Role 2: <input class="widefat" id="<?php echo $this->get_field_id('role2'); ?>" name="<?php echo $this->get_field_name('role2'); ?>" type="text" value="<?php echo attribute_escape($role2); ?>" /></label></p>

			  <p><label for="<?php echo $this->get_field_id('link'); ?>">Read More Link: <input class="widefat" id="<?php echo $this->get_field_id('link'); ?>" name="<?php echo $this->get_field_name('link'); ?>" type="text" value="<?php echo attribute_escape($link); ?>" /></label></p>
			<?php
        }

        public function update( $new_instance, $old_instance ) {
		    $instance = $old_instance;
		    $instance['image'] = $new_instance['image'];
		    $instance['name'] = $new_instance['name'];
		    $instance['role'] = $new_instance['role'];
		    $instance['role2'] = $new_instance['role2'];
		    $instance['link'] = $new_instance['link'];
		    return $instance;
        }

        public function widget( $args, $instance ) {
		    extract($args, EXTR_SKIP);
		 
		    echo $before_widget;
		    $image = empty($instance['image']) ? '' : apply_filters('widget_title', $instance['image']);
		    $name = empty($instance['name']) ? '' : apply_filters('widget_title', $instance['name']);
		    $role = empty($instance['role']) ? '' : apply_filters('widget_title', $instance['role']);
		    $role2 = empty($instance['role2']) ? '' : apply_filters('widget_title', $instance['role2']);
		    $link = empty($instance['link']) ? '' : apply_filters('widget_title', $instance['link']);
		 
		    echo "<div class='leadership-box'>";

		    if (!empty($image))
		      echo "<div class='icon'><img src='".get_stylesheet_directory_uri()."/library/images/".$image."' alt='Leadership image'></div>";
		
		    if (!empty($name))
		      echo "<h4>".$name."</h4>";

		  	echo "<div class='description'>";

		    if (!empty($role))
		      echo "<p>".$role."</p>";

		    if (!empty($role2))
		      echo "<p>".$role2."</p>";

		    echo "</div>";   

		    if (!empty($link))
		      echo "<a href='".$link."' alt='Read More'>Read More</a>";

		  	echo "</div>";

		    echo $after_widget;
        }

}
register_widget( 'LeadershipBoxWidget' );

/************* SERVICES BOXES *****************/

class ServicesBoxWidget extends WP_Widget {
         public function __construct() {
               parent::WP_Widget(false,'Services Box','description=Services Boxes');
        }

        public function form( $instance ) {
			$instance = wp_parse_args( (array) $instance, array( 'image' => '', 'title' => '', 'description' => '', 'link' => '' ) );
			    $image = $instance['image'];
			    $title = $instance['title'];
			    $description = $instance['description'];
			    $link = $instance['link'];
			?>
			  <p><label for="<?php echo $this->get_field_id('image'); ?>">Image: <input class="widefat" id="<?php echo $this->get_field_id('image'); ?>" name="<?php echo $this->get_field_name('image'); ?>" type="text" value="<?php echo attribute_escape($image); ?>" /></label></p>

			  <p><label for="<?php echo $this->get_field_id('title'); ?>">Title: <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo attribute_escape($title); ?>" /></label></p>

			  <p><label for="<?php echo $this->get_field_id('description'); ?>">Description: </label><textarea class="widefat" id="<?php echo $this->get_field_id('description'); ?>" name="<?php echo $this->get_field_name('description'); ?>" /><?php echo attribute_escape($description); ?></textarea></p>

			  <p><label for="<?php echo $this->get_field_id('link'); ?>">Read More Link: <input class="widefat" id="<?php echo $this->get_field_id('link'); ?>" name="<?php echo $this->get_field_name('link'); ?>" type="text" value="<?php echo attribute_escape($link); ?>" /></label></p>
			<?php
        }

        public function update( $new_instance, $old_instance ) {
		    $instance = $old_instance;
		    $instance['image'] = $new_instance['image'];
		    $instance['title'] = $new_instance['title'];
		    $instance['description'] = $new_instance['description'];
		    $instance['link'] = $new_instance['link'];
		    return $instance;
        }

        public function widget( $args, $instance ) {
		    extract($args, EXTR_SKIP);
		 
		    echo $before_widget;
		    $image = empty($instance['image']) ? '' : apply_filters('widget_title', $instance['image']);
		    $title = empty($instance['title']) ? '' : apply_filters('widget_title', $instance['title']);
		    $description = empty($instance['description']) ? '' : apply_filters('widget_title', $instance['description']);
		    $link = empty($instance['link']) ? '' : apply_filters('widget_title', $instance['link']);
		 
		    echo "<div class='box-services'>";

		    if (!empty($image))
		      echo "<div class='icon'><img src='".get_stylesheet_directory_uri()."/library/images/".$image."' alt='".$title."'></div>";
		
		    if (!empty($title))
		      echo "<h5>".$title."</h5>";

		    if (!empty($description))
		      echo "<p>".$description."</p>";

		    if (!empty($link))
		      echo "<a href='".$link."' alt='Read More'>Read More</a>";

		  	echo "</div>";

		    echo $after_widget;
        }

}
register_widget( 'ServicesBoxWidget' );

?>
