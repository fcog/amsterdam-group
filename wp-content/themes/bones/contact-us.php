<?php
/*
Template Name: Contact Us
*/
?>

<?php get_header(); ?>

<div id="info-container" class="container_24">

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

            <h2><?php the_title(); ?></h2>
            <img src="<?php echo get_template_directory_uri(); ?>/library/images/google-maps.jpg">
            <div class="dark-gray-content">
                <?php
                if (is_active_sidebar('contact-us-form')) :
                    dynamic_sidebar('contact-us-form');
                endif;
                ?> 
                <div class="contact-info">
                    <?php
                    if (is_active_sidebar('contact-us-info')) :
                        dynamic_sidebar('contact-us-info');
                    endif;
                    ?>                 
    <!--                 <h3>Address</h3>
                    <li class="location">1301 U Street, Suite 420 Washington DC, 20009, United States</li>
                    <h3>Contact</h3>
                    <li class="t-number">42 556 430 6660<br />
                    42 556 430 6660</li>
                    <li class="mail-support"><a href="#" title="support">support@amsterdamgroup.net</a><br />
                    <a href="#" title="info">info@amsterdamgroup.net</a></l1> -->
                </div>
            </div>

    <?php endwhile; endif ?>

</div>

<?php get_footer(); ?>
