<?php
/*
Template Name: Services
*/
?>

<?php get_header(); ?>

<div id="info-container" class="container_24">

  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <h2><?php the_title(); ?></h2>
      <div class="transparent-content">
        <p><?php the_content(); ?></p>
      </div>
    <div class="white-content">
      <div id="services-wrapper">
        <?php
        if (is_active_sidebar('services')) :
            dynamic_sidebar('services');
        endif;
        ?>
      </div>       
    </div>

  <?php endwhile; endif ?>

</div>  

<?php get_footer(); ?>
