<?php
/*
Template Name: About Us Internal
*/
?>

<?php get_header(); ?>

  <div id="info-container" class="container_24">

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <h2>About Us</h2>
      <section class="sidebar">
        <?php $image = get_field('profile_image' ); ?>
        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"/>
  <ul class="contact-leadership">
        <li class="tel"><?php echo get_field('phone'); ?></li>
            <li class="mail"><a href="mailto:<?php echo get_field('email'); ?>"><?php echo get_field('email'); ?></a></li>
            <li class="twitter-profile"><a href="http://<?php echo get_field('twitter'); ?>"><?php echo get_field('twitter'); ?></a></li>
        </ul>
    </section>
    <div class="white-content grid_16 alpha omega" >
      <h3><?php the_title(); ?></h3>
      <h4><?php echo get_field('role'); ?></h4>
      <p><?php the_content(); ?></p>
  <a href="../about-us" title="back to About us" class="green-buttom">Back</a>
      </div>

  <?php endwhile; endif ?>
  
  </div>    

<?php get_footer(); ?>
